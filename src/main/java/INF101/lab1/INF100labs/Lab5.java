package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        
        ArrayList<Integer> myMultipliedList = new ArrayList<>();
        
        for (int num : list){
            myMultipliedList.add(num*2);
        }

        return myMultipliedList;



        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        
        ArrayList<Integer> myCulledList = new ArrayList<>();
        
        for (int num : list){
            if (num == 3){

            }
            else {
                myCulledList.add(num);
            }
        }

        return myCulledList;
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        
        ArrayList<Integer> myUniqueList = new ArrayList<>();
        
        for (int num : list){
            if (myUniqueList.contains(num)){

            }
            else {
                myUniqueList.add(num);
            }
        }

        return myUniqueList;
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        
        for (int i = 0; i < a.size(); i = i+1){
            a.set (i, a.get(i) + b.get(i));
        }

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}