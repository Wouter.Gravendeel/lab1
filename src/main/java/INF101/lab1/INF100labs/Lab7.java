package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
        grid.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));
        allRowsAndColsAreEqualSum(grid);

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        //int i = 0;
        //ArrayList<ArrayList<Integer>> gridTemp = new ArrayList<>();
        
        //for (ArrayList list : grid){
        //    if (i == row){
        //        i = i+1;
        //    }
        //    else {
        //        gridTemp.add(new ArrayList<>(list));
        //        i = i+1;
        //    }
        //}


        grid.remove(row);

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        
        boolean sameSum = true;

        int testValue = 0;
        ArrayList<Integer> columnSums = new ArrayList<>();
        ArrayList<Integer> testLine = grid.get(0);
        for (int j = 0; j < testLine.size(); j = j+1){
            testValue = testValue + testLine.get(j);
            columnSums.add(0);
        }
                

        for (int i = 0; i < grid.size(); i = i+1){
            ArrayList<Integer> line = grid.get(i);
            
            
            int testedValue = 0;

            for (int j = 0; j < line.size(); j = j+1){
                testedValue = testedValue + line.get(j);
                columnSums.set (j, columnSums.get(j) + line.get(j));
                
            }

            if (testValue == testedValue){

            }

            else{
                sameSum = false;
            }
            System.out.println(columnSums.size());
            
        
        }

        for (int k = 0; k < columnSums.size(); k=k+1){
            System.out.println(columnSums.get(k));
                
            if (columnSums.get(k) == columnSums.get(0)){

            }
            else{
                sameSum = false;
            }
        }
        
        return sameSum;

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}