package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
        int longestWordLength = word1.length();

        // finner lengde på de lengste ordet
        if (word2.length() > longestWordLength){
            longestWordLength = word2.length();
        }

        if (word3.length() > longestWordLength){
            longestWordLength = word3.length();
        }

        // Printer alle ordene som har størst lengde
        if (word1.length() == longestWordLength){
            System.out.println(word1);
        } 
        if (word2.length() == longestWordLength){
            System.out.println(word2);
        } 
        if (word3.length() == longestWordLength){
            System.out.println(word3);
        } 
                
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isLeapYear(int year) {
        
        boolean leapYear = false;

        if (year % 400 == 0){
            return true;
        }

        else if (year % 100 == 0){
            return false;
        }

        else if (year % 4 == 0){
            return true;
        }

        else {
            return false;
        }

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isEvenPositiveInt(int num) {

        if (num % 2 == 0 && num > 0){
            return true;
        }
        else{
            return false;
        }

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}
