package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i=0; i<=n; i=i+1){
            if (i == 0 ){
                
            }

            else if (i%7 == 0){
                System.out.println(i);
            }
        }
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void multiplicationTable(int n) {
        if (n <= 0){

        }
        else {
            for (int i=1; i<=n; i=i+1){
                String newLine = i + ":";
                for (int j=1; j<=n; j=j+1){
                    newLine += " " + i*j;
                }
                System.out.println(newLine);
            }
        }
        
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static int crossSum(int num) {
        String tempString = Integer.toString(num);
        int crossSumTemp = 0;
        for (int i = 0 ; i < tempString.length(); i = i+1){
            char oneDigit = tempString.charAt(i);
            crossSumTemp += Character.getNumericValue(oneDigit);
        }
        return crossSumTemp;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}